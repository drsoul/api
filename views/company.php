<?
defined('EXEC') or die('No direct access!');

foreach($data['content'] as $key => $value){
    if(is_array($value)){
        echo '<item>'.chr(10);
        foreach($value as $key_1 => $value_1){
            echo '<'.$key_1.'>'.$value_1.'</'.$key_1.'>'.chr(10);
        }
        echo '</item>'.chr(10);
    }else{
        echo '<'.$key.'>'.$value.'</'.$key.'>'.chr(10);
    }
}