<?
defined('EXEC') or die('No direct access!');

echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>

<response>
<data>
<? include \core\reg::get('root').'/views/'.$content_view; ?></data>    
<status><?=$data['status'];?></status>    
<err_msg><? if(!empty($data['errors'][0])){echo $data['errors'][0];} ?></err_msg>
</response>