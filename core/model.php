<?php
namespace core;

defined('EXEC') or die('No direct access!');

class model{
    
    protected $tbPrefix = 'cms_';
    protected $displaySql = 0;
    protected $displayQueryParams = 0;    
    protected $executeSqlSelect = 1;
    protected $executeSqlInsert = 1;
    protected $executeSqlUpdate = 1;
    protected $executeSqlDelete = 1;
    protected $bCore;
    protected $errors;
    protected $status = 0;    
    
    public function __construct(){
        
        if(!defined('INC_ROOT')){
            define('INC_ROOT', reg::get('root').'/../inc/');
        }
        __autoload('/../config.php'); 
        __autoload('/../inc/core.class.php');
        
        $this->setBCore(new \Core());
    }
    
    public function validateDomain($domain){
        $domain = trim($domain);
        $preg = '~^http://(www.)?~i';
        $site = preg_replace($preg, '', $domain);
        $site = explode("/", $site);
        $site = $site[0];

        $bCore = $this->getBCore();
        $site = $bCore::idna()->encode($site);
        
        if($bCore->is_valid_domain_name($site)){
            return $site;
        }else{
            return false;
        }
    }
    
    protected function setBCore($core){
        $this->bCore = $core;
    }
    
    public function getBCore(){
        return $this->bCore;
    }
    
    public function getDisplaySql(){
        return $this->displaySql;
    }
    
    public function getDisplayQueryParams(){
        return $this->displayQueryParams;
    }    
    
    public function getExecuteSqlSelect(){
        return $this->executeSqlSelect;
    }
    
    public function getExecuteSqlInsert(){
        return $this->executeSqlInsert;
    }
    
    public function getExecuteSqlUpdate(){
        return $this->executeSqlUpdate;
    }
    
    public function getExecuteSqlDelete(){
        return $this->executeSqlDelete;
    }
    
    public function authorization($email, $password){
        
        __autoload('/models/user.php');
        
        $user = new \models\user();

        $user_id = $user->authWrap($email, $password);
        
        if($user_id != true){return false;}
        return $user_id;
    }
    
    /**
     * 
     * @param type $user_id
     * @param type $id
     * @param type $authLvl
     * @return boolean
     */
    public function userAuthComAdsTar($user_id, $id, $authLvl=1){
        
        $result = array();
        $result['target_id'] = 0;
        $result['ads_id'] = 0;
        $result['company_id'] = 0;
        
        #validate user_id
        if(empty($user_id)){
            $this->setErrors('Not set user_id.');
            return false;
        }
        
        if($authLvl == 2){
            $target_id = $id;
        }elseif($authLvl == 1){
            $ads_id = $id;
        }elseif($authLvl == 0){
            $company_id = $id;
        }
        
        #auth target
        if($authLvl >= 2){
            #validate target_id
            if(empty($target_id)){
                $this->setErrors('Not set target_id.');
                return false;
            }  
            $ads = $this->getAdsId($target_id, $setError = 1);
            $ads_id = $ads['ads_id'];
            if($ads_id == false){return false;}
            
            $result['target_id'] = $target_id;
            $result['ads_id'] = $ads_id;
        }
        
        #auth ads
        if($authLvl >= 1){
            
            if(empty($ads_id)){
                $this->setErrors('Not set ads_id.');
                return false;
            }   
            
            #validate ads_id
            __autoload('/models/ads.php');
            $ads = new \models\ads();
            $row = $ads->getCompanyId($ads_id, $setError = 1);        
            if($row['company_id'] == false){return false;}
            $company_id = $row['company_id'];
            
            $result['company_id'] = $company_id;
        }
        
        if($authLvl >= 0){
            
            if(empty($company_id)){
                $this->setErrors('Not set company_id.');
                return false;
            }   
            
            #validate company_id
            __autoload('/models/company.php');
            $company = new \models\company();
            if($company->userOwnCompany($user_id, $company_id, $setError = 1) == false){                
                return false;                
            } 
                        
        }
        
        return $result;        
    }
    
    /**
     * 
     * @return type
     */
    public function lastId(){
        return $this->getBCore()->dbLastId();
    }
    
    /**
     * 
     * @param type $text
     * @return type
     */
    public function esc($text){
        $text = trim($text);
        return mysql_real_escape_string($text);
    }
    
    /**
     * 
     * @param type $paramsInt
     * @param type $setError
     * @param type $setFalse
     * @return boolean|array
     */
    public function testIntParams($paramsInt, $setError=1, $setFalse=1){
            
        $paramsResult = array();
        foreach($paramsInt as $key => $value){
            $res = $this->toInt($value);
            
            if($res === false){
                if($setError == 1){
                    $this->setErrors('Not set '.$key.'.');
                }
                if($setFalse == 1){
                    return false;
                }                
            }
            $paramsResult[$key]=$res;
        }
        return $paramsResult;
    }
    
    /**
     * 
     * @param type $str
     * @return boolean
     */
    public function toInt($str){
        if(is_numeric($str)){
            return (int)$str;
        }
        return false;        
    }
    
    /**
     * 
     * @param type $select
     * @param type $table
     * @param type $where
     * @param type $where_params
     * @param type $type
     * @return string
     */
    public function select($select = array(), $table, $where_pattern, $where_params = array(), $type='one', $prefix = 1, $join = ''){
        
        $where_params = $this->escArr($where_params);
        //echo '<br><br><br>'.$where_pattern.'<br><br><br>';
        //echo '<br><br><br>'.print_r($where_params).'<br><br><br>';
        foreach($where_params as $value){
            $where_pattern = preg_replace('~\:\?~isu', $value, $where_pattern, 1);
        }           
        
        if(strpos($where_pattern, ':?') !== false){
            $this->setErrors('Error. Not all error parameters are replaced.');
            return false;
        }
        
        if($prefix == 1){
            $prefix = $this->getTbPrefix();
        }else{
            $prefix = '';
        }
        
        $joinString = '';
        if(!empty($join)){
            $joinString = ' '.$join.' ';
        }
        
        $sql = 'SELECT '.implode(',', $select).' FROM '.$prefix.$table.'
            '.$joinString.'
            WHERE '.$where_pattern;
        
        if($this->getDisplaySql() == 1){            
            echo $this->sqlView($sql);
        }
        
        if($this->getExecuteSqlSelect()){
            if($type == 'one'){
                return $this->getBCore()->dbFetchOne($sql);            
            }else{
                return $this->getBCore()->dbFetchAll($sql);
            }
        }else{
            $this->setErrors('Select execute disabled.');
            return false;
        }
    }    
    
    /**
     * 
     * @param type $sql
     * @return type
     */
    public function sqlView($sql){
        return chr(10).'<br><br><br>SQL: '.$sql.'<br><br><br>'.chr(10); 
    }
        
    /**
     * 
     * @param type $insert
     * @param type $table
     * @param type $prefix
     * @return boolean
     */
    public function insert($insert = array(), $table, $prefix = 1, $onDuplicateKey = ''){
                
        if($prefix == 1){
            $prefix = $this->getTbPrefix();
        }else{
            $prefix = '';
        }
        
        $duplicateKeyString = '';
        if(!empty($onDuplicateKey)){
            $duplicateKeyString = ' ON DUPLICATE KEY UPDATE '.$onDuplicateKey;
        }
        
        $sql = 'INSERT INTO '.$prefix.$table.' ('.implode(',', array_keys($insert)).') 
            VALUES (\''.implode('\',\'', $insert).'\')'.$duplicateKeyString;
        
        if($this->getDisplaySql() == 1){
            echo $this->sqlView($sql);
        }
        
        if($this->getExecuteSqlInsert()){
            if (!$this->getBCore()->dbSql($sql)){
                $this->setErrors('Insert error.');
                return false;
            }
            return $this->getBCore()->dbLastId();
        }else{
            $this->setErrors('Insert execute disabled.');
            return false;
        }
    }
        
    /**
     * 
     * @param type $insert
     * @param type $table
     * @param type $prefix
     * @param type $onDuplicateKey
     * @return boolean
     */
    public function insertMass($insert = array(), $table, $prefix = 1, $onDuplicateKey = ''){
        
        if($this->getDisplayQueryParams() == 1){
            echo 'insertMass: ';
            echo '<pre>';
            print_r($insert);   
            echo '</pre>';
        }
        
        if($prefix == 1){
            $prefix = $this->getTbPrefix();
        }else{
            $prefix = '';
        }
        
        $duplicateKeyString = '';
        if(!empty($onDuplicateKey)){
            $duplicateKeyString = ' ON DUPLICATE KEY UPDATE '.$onDuplicateKey;
        }
        
        $values = '';
        $sep = '';
        $keys = '';
        foreach($insert as $key => $value){
            if(empty($keys)){
                $keys = implode(',', array_keys($value));
            }
            $values .= $sep.'(\''.implode('\',\'', $value).'\')';
            $sep = ',';
        }
        
        $sql = 'INSERT INTO '.$prefix.$table.' ('.$keys.') 
            VALUES '.$values.$duplicateKeyString;
        
        if($this->getDisplaySql() == 1){
            echo $this->sqlView($sql);
        }        
        
        if($this->getExecuteSqlInsert()){
            $res = $this->getBCore()->dbSql($sql);
            if (!$res){
                $this->setErrors('Insert error.');
                return false;
            }
            return $res;
        }else{
            $this->setErrors('Insert execute disabled.');
            return false;
        }
    }
    
    /**
     * 
     * @param type $update
     * @param type $table
     * @param type $where_pattern
     * @param type $where_params
     * @param type $prefix
     * @return boolean
     */
    public function update($update = array(), $table, $where_pattern,
            $where_params = array(),  $prefix = 1){
                
        $where_params = $this->escArr($where_params);
        //echo '<br><br><br>'.$where_pattern.'<br><br><br>';
        //echo '<br><br><br>'.print_r($where_params).'<br><br><br>';
        foreach($where_params as $value){
            $where_pattern = preg_replace('~\:\?~isu', $value, $where_pattern, 1);
        }           
        
        if(strpos($where_pattern, ':?') !== false){
            $this->setErrors('Error. Not all error parameters are replaced.');
            return false;
        }
        
        if($prefix == 1){
            $prefix = $this->getTbPrefix();
        }else{
            $prefix = '';
        }
        
        $set = '';
        $sep = '';
        foreach($update as $key => $value){
            $set .= $sep.$key.'=\''.$value.'\'';
            $sep = ',';
        }
        
        $sql = 'UPDATE '.$prefix.$table.' SET '.$set. ' WHERE '.$where_pattern;
        
        if($this->getDisplaySql() == 1){
            echo $this->sqlView($sql);
        }
                
        if($this->getExecuteSqlUpdate()){
            $res = $this->getBCore()->dbSql($sql);
            if (!$res){
                $this->setErrors('Update error.');
                return false;
            }
            return $res;
        }else{
            $this->setErrors('Update execute disabled.');
            return false;
        }
        
    }
    
    /**
     * 
     * @param type $table
     * @param type $where_pattern
     * @param type $where_params
     * @param type $prefix
     * @return boolean
     */
    public function delete($table, $where_pattern,
            $where_params = array(),  $prefix = 1){
                
        $where_params = $this->escArr($where_params);
        //echo '<br><br><br>'.$where_pattern.'<br><br><br>';
        //echo '<br><br><br>'.print_r($where_params).'<br><br><br>';
        foreach($where_params as $value){
            $where_pattern = preg_replace('~\:\?~isu', $value, $where_pattern, 1);
        }           
        
        if(strpos($where_pattern, ':?') !== false){
            $this->setErrors('Error. Not all error parameters are replaced.');
            return false;
        }
        
        if($prefix == 1){
            $prefix = $this->getTbPrefix();
        }else{
            $prefix = '';
        }
        
        $sql = 'DELETE FROM '.$table.' WHERE '.$where_pattern;
        if(empty($where_pattern)){
            $sql = '';//если нет условия, удаляем запрос. На всякий случай.
        }
        
        if($this->getDisplaySql() == 1){
            echo $this->sqlView($sql);
        }
        
        
        if($this->getExecuteSqlDelete()){
            $res = $this->getBCore()->dbSql($sql);
            if (!$res){
                $this->setErrors('Delete error.');
                return false;
            }
            return $res;
        }else{
            $this->setErrors('Delete execute disabled.');
            return false;
        }
    }    
    
    /**
     * 
     * @param type $params
     * @return string
     */
    public function escArr($params){
        foreach($params as $key => $value){
            $params_res[$key] = "'".$this->esc($value)."'";
        }
        return $params_res;
    }
    
    /**
     * 
     * @return boolean
     */
    public function checkErrors(){
        if(!empty($this->getErrors())){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * @return type
     */
    protected function getTbPrefix(){
        return $this->tbPrefix;
    }
    
    /**
     * 
     * @return type
     */
    public function getErrors(){
        return reg::getErrors();
    }
    
    /**
     * 
     * @return type
     */
    public function getStatus(){        
        return reg::getStatus();
    }
    
    /**
     * 
     * @param type $error
     * @param type $status
     */
    public function setErrors($error, $status = 1){        
        reg::setErrors($error);
        reg::setStatus($status);
    } 
    
    /**
     * 
     * @param type $arrayData
     * @param type $arrayFilter
     * @param type $addElem
     * @param type $validValueElemInt
     * @return boolean
     */
    #пропускает элементы 2го уровня массива $arrayData в итоговый массив
    #по значениям массива $arrayFilter которые являются ключами для массива $arrayData.
    #$arrayFilter - содержит допустимые ключи
    #$validValueElemInt - приводить отфильтрованные по ключам значения к int или нет
    #$addElem - добавочные элементы на каждый уровень массива $arrayData
    public function filterArray($arrayData, $arrayFilter, $addElem = array(), $validValueElemInt = 1){
        $arrayFilterKeys = array_keys($arrayFilter);
        $arrayDataNew = array();
        foreach($arrayData as $key => $value){
            
            $arrayDataElem = array();
            foreach($value as $key_1 => $value_1){                
                if(in_array($key_1, $arrayFilterKeys)){
                    if($validValueElemInt == 1){
                        $value_tmp = (int)$value_1;                                
                    }else{
                        $value_tmp = $value_1;
                    }
                    $arrayDataElem[$key_1] = $value_tmp;
                }
            }
            
            foreach($addElem as $key_1 => $value_1){
                if($validValueElemInt == 1){
                    $value_tmp = (int)$value_1;                                
                }else{
                    $value_tmp = $value_1;
                }           
                $arrayDataElem[$key_1] = $value_tmp;
            }
            
            $arrayDataNew[] = $arrayDataElem;
        }
        
        if($arrayDataNew == false){
            return false;
        }
        return $arrayDataNew;
    }
}