<?php
namespace core;

defined('EXEC') or die('No direct access!');

class controller{
    
    public $model;
    public $view;
    
    public function __construct() {

        $this->view = new view();        
        $className = $this->whoiam();
        if($className == 'controller'){
            $classNameWithNamespace = '\core\model';
            __autoload('/core/model.php');        
            $this->model = new $classNameWithNamespace;
        }else{
            $classNameWithNamespace = '\models\\'.$className;
            __autoload('/models/'.$className.'.php');        
            $this->model = new $classNameWithNamespace;
        }
    }    
    
    /**
     * 
     * @param type $params
     */
    protected function addViewJSON($params = array()){
        
        $data['content'] = $this->model->add($params);         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }  
    
    /**
     * 
     * @param type $params
     */
    public function editViewJSON($params = array()){
        
        $data['content'] = $this->model->edit($params);         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    } 
    
    /**
     * 
     * @param type $params
     */
    public function getViewJSON($params = array()){        

        $data['content'] = $this->model->get($params);  
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }
    
    /**
     * 
     * @param type $params
     */
    public function setActivityViewJSON($params = array()){
        
        $data['content'] = $this->model->setActivity($params);         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }
    
    /**
     * 
     * @param type $params
     */
    public function getActivityViewJSON($params = array()){
        
        $data['content'] = $this->model->getActivity($params);         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }
    
    /**
     * 
     * @param type $params
     */
    public function delViewJSON($params = array()){
        
        $data['content'] = $this->model->del($params);         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }
    
    /**
     * 
     * @param type $params
     */
    public function routerErrorViewJSON($params = array()){
        
        $data['content'] = $params['content'];         
        $data = $this->addStatusAndErrors($data);
        $this->view->render('templateJSON.php', $data);
    }
    
    /**
     * 
     * @param type $data
     * @return type
     */
    public function addStatusAndErrors($data = array()){
        
        $data['status'] = $this->model->getStatus();
        $data['errors'] = $this->model->getErrors();
        
        return $data;
    }
    
    /**
     * 
     * @return type
     */
    public function whoiam(){
        
        $str = get_class($this); //  получаем имя текущего класса
        $temp = explode('\\', $str); //  разделяем строку по символу "\"
        return end($temp); // выбираем последний элемент массива
    }

}