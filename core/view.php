<?php
namespace core;

defined('EXEC') or die('No direct access!');

class view{
 
    /**
     * 
     * @param type $content_view
     * @param type $template_view
     * @param type $data
     */
    public function render($template_view, $data = null, $content_view = null){
        
        include reg::get('root').'/views/'.$template_view;
    }
}