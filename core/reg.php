<?php
namespace core;

defined('EXEC') or die('No direct access!');

class reg{
    
    static protected $data = array();
    static protected $errors = array();
    static protected $status = 0;
    
    static public function setStatus($value){
        self::$status = $value;
    }
    
    static public function getStatus(){
        return self::$status;
    }
    
    /**
     * 
     * @param type $value
     */
    static public function setErrors($value){
        self::$errors[] = $value;
    }
    
    /**
     * 
     * @return type
     */
    static public function getErrors(){
        return self::$errors;
    }
    
    /**
     * 
     * @param type $key
     * @param type $value
     */
    static public function set($key, $value){
        self::$data[$key] = $value;
    }
    
    /**
     * 
     * @param type $key
     * @return type
     */
    static public function get($key){
        return isset(self::$data[$key]) ? self::$data[$key] : null;
    }
    
    /**
     * 
     * @param type $key
     */
    static public function remove($key){
        if(isset(self::$data[$key])){
            unset(self::$data[$key]);
        }
    }
}