<?php
namespace core;

defined('EXEC') or die('No direct access!');

class Router{
    protected $request;
    protected $config;
    protected $routes;
    protected $action_name;
    protected $class_name;
    protected $class_name_with_namespace;
    protected $controller_file;
    protected $errors;
    protected $controller;
    protected $queryString;
    private static $instance;
    
    public static function I(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    
    private function __construct() {        
        
        $this->routes = __autoload('/config/routes.php');
        __autoload('/core/controller.php');
        __autoload('/core/model.php');
        __autoload('/core/view.php'); 
        
        if(\config\apiConfig::I()->getMethodHTTP() == 'GET'){
            $this->request = $_GET;
        }else{
            $this->request = $_POST;
        }         
        
        if(empty($this->request)){            
            $this->setErrors("Query no have params.");
            return false;
        }     

    }
    
    private function __clone(){}
    
    /**
     * 
     * @return type
     */
    public function getErrors(){
        return reg::getErrors();
    }
    
    /**
     * 
     * @return type
     */
    public function getStatus(){        
        return reg::getStatus();
    }
    
    /**
     * 
     * @param type $error
     * @param type $status
     */
    public function setErrors($error, $status = 1){        
        reg::setErrors($error);
        reg::setStatus($status);
    }   
    
    /**
     * 
     */
    public function run(){
        if($this->execute() === false AND \config\apiConfig::I()->displayErrors() == 1){
            /*
            foreach ($this->getErrors() as $error){
                echo '<br>'.$error;                
            }
            */
            $coreController = new \core\controller();
            $coreController->routerErrorViewJSON(
                    $params = array(
                        'content'=>'',
                        'status'=>$this->getStatus(),
                        'errors'=>$this->getErrors()
                    )
                    );
        }
    }
    
    /**
     * 
     * @return boolean
     */
    protected function execute(){
        
        if(!array_key_exists($this->request['action'], $this->routes)){
            if(empty($this->request['action'])){
                $this->setErrors('Empty action.');
            }else{
                $this->setErrors("Not exist '".$this->request['action']."' action.");        
            }
            return false;
        }
               
        $this->action_name = $this->routes[$this->request['action']]['method'];
        $this->class_name = $this->routes[$this->request['action']]['class'];
        $this->class_name_with_namespace = 'controllers\\'.$this->routes[$this->request['action']]['class'];
        
        $this->controller_file = ROOT.'/controllers/'.$this->class_name.'.php';
        
        if(!file_exists($this->controller_file)){            
            $this->setErrors("Not exist '$this->controller_file' controller file.");
            return false;
        }
        
        include($this->controller_file);
        
        if(!class_exists($this->class_name_with_namespace)){            
            $this->setErrors("Not exist '$this->class_name_with_namespace' controller.");
            return false;
        } 
        
        $reflector = new \ReflectionClass($this->class_name_with_namespace);
        if(!$reflector->HasMethod($this->action_name)){            
            $this->setErrors("Controller no have '$this->action_name' method.");
            return false;
        }

        $params = $this->request;
        unset($params['action']);        
       
        $this->controller = new $this->class_name_with_namespace;
        
        call_user_func(array($this->controller, $this->action_name), $params);
        
        return true;
    }
}