<?php
namespace controllers;

defined('EXEC') or die('No direct access!');

class target extends \core\controller{
    
    /**
     * 
     * @param type $params
     */
    public function add($params = array()){
        parent::addViewJSON($params);
    }  
    
    /**
     * 
     * @param type $params
     */
    public function get($params = array()){
        parent::getViewJSON($params);
    } 
    
    /**
     * 
     * @param type $params
     */
    public function edit($params = array()){
        parent::editViewJSON($params);
    } 
    
    /**
     * 
     * @param type $params
     */
    public function setActivity($params = array()){
        parent::setActivityViewJSON($params);
    } 
    
    /**
     * 
     * @param type $params
     */
    public function getActivity($params = array()){
        parent::getActivityViewJSON($params);
    } 
    
    /**
     * 
     * @param type $params
     */
    public function del($params = array()){
        parent::delViewJSON($params);
    }
    

}
