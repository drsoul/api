<?php
namespace controllers;

defined('EXEC') or die('No direct access!');

class user extends \core\controller{    

    /**
     * 
     * @param type $params
     */
    public function add($params = array()){
        parent::addViewJSON($params);
    }        

    /**
     * 
     * @param type $params
     */
    public function get($params = array()){
        parent::getViewJSON($params);
    } 
}
