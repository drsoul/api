<?php
set_error_handler('logError');

//обработчик ошибок
function logError($errno, $errstr = '', $errfile = '', $errline = ''){
    $dir = dirname(__FILE__) . '/';
    $file = 'logs_' . date('Y_m_d').'.txt';
    
    file_put_contents($dir.$file,$errno."|$errstr|$errline|$errfile\n",FILE_APPEND);    
}

error_reporting(E_ERROR | E_WARNING | E_NOTICE);
ini_set('display_errors', 0); 

define('ROOT', dirname(__FILE__));
define('EXEC', 1);

include ROOT.'/core/reg.php';
\core\reg::set('root', ROOT);

function __autoload($pathToClass){
    return include_once \core\reg::get('root').$pathToClass;  
}

__autoload('/config/apiConfig.php');
__autoload('/core/router.php');

\core\Router::I()->run();