<?php
namespace models;

defined('EXEC') or die('No direct access!');

class user extends \core\model{
    
    static protected $users = array();
    
    /**
     * 
     * @param type $email
     * @param type $password
     * @return boolean or $lastId
     */
    public function add($params){
        
        $email=$params['email'];
        $password=$params['password'];
        
        if(!$this->validEmailWrap($email)){return false;}        
        if(!$this->validPasswordWrap($password)){return false;}        
                
        $password= md5($password); //min length 6
        $created = date('Y-m-d H:i:s');
        $auth_key = md5(time());
        $status = '1';    
        
        $row = $this->select($select=array('id'),
            $table='users',
            $where_pattern='email=:?',
            $where_params=array($email),
            $type='one',
            $prefix=1,
            $join='');

        if(!empty($row['id'])){
            $this->setErrors('This user aready has.');
            return $row['id'];
        }
        
        $user_id = $this->insert(
            $insert=array('email' => $email, 'password' => $password,
            'created' => $created, 'auth_key' => $auth_key, 'status' => $status),
            $table='users',
            $prefix=1); 
        if($user_id != true){
            $this->setError('Error insert users.');
            return false;
        }
        return $user_id;
    }
        
    /**
     * 
     * @param type $email
     * @return boolean
     */
    protected function validEmail($email){
        
        $pattern = "/^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2,4}|museum|travel)$/i";
        
        if(preg_match($pattern, $email)){
            return true;
        }else{            
            return false;
        }
    }
    
    /**
     * 
     * @param type $password
     * @return boolean
     */
    protected function validPassword($password){
        
        $password_len = mb_strlen($password, 'utf-8');
        if($password_len < 6 OR $password_len > 30){            
            return false;
        }else{
            return true;
        }
    }   
    
    /**
     * 
     * @param type $email
     * @return boolean or $userId
     */
    public function get($params){
        
        $email=$params['email'];
        $password=$params['password'];
        
        if(!$this->validEmailWrap($email)){return false;}        
        if(!$this->validPasswordWrap($password)){return false;}
        
        $row = $this->select($select = array('id', 'email', 'status', 'balance'),
                $table = 'users',
                $where_pattern = 'email=:? AND password=:?',
                $where_params = array($email, md5($password)),
                $type = 'one');
        
        if(empty($row['id'])){
            $this->setErrors('Incorrect email or password.');
            return false;
        }        
        return $row;
    }
    
    /**
     * 
     * @param type $email
     * @return boolean
     */
    protected function validEmailWrap($email){

        if(!$this->validEmail($email)){
            $this->setErrors('Not valid email.');
            return false;
        }
        return true;
    }    
    
    /**
     * 
     * @param type $password
     * @return boolean
     */
    protected function validPasswordWrap($password){
        if(!$this->validPassword($password)){
            $this->setErrors('Not valid password, need 6 > password < 30.');
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param type $email
     * @param type $password
     * @return boolean or $row['id']
     */
    public function auth($email, $password){
        
        $params['email']=$email;
        $params['password']=$password;
        
        $row = $this->get($params);   

        if($row['id'] == true){return $row['id'];}
        return false;
    }
    
    /**
     * 
     * @param type $email
     * @param type $password
     * @return boolean or $row['id']
     */
    public function authWrap($email, $password){   

        $user_id = $this->getUserId($email, $password);

        if($user_id == false){
            
            $user_id = $this->auth($email, $password);   
            
            if($user_id != true){
                $this->setErrors('Authorization failed.');
                return false;
            }
            $this->setUserId($email, $password, $user_id);
        }

        return $user_id;
    }
    
    public function setUserId($email, $password, $user_id){
        self::$users[$email.'|'.$password] = $user_id;

    }
    
    public function getUserId($email, $password){
        if(isset(self::$users[$email.'|'.$password])){
            return self::$users[$email.'|'.$password];
        }

        return false;
    }
}