<?php
namespace models;

defined('EXEC') or die('No direct access!');

class ads extends \core\model{
    
    /**
     * 
     * @param type $params
     * @return boolean or $ads_id
     */
    # $params = array('email'=>,'password'=>,'tpl'=>,'company_id'=>,
    # 'name'=>,'description'=>,'text_button'=>,'link'=>);
    public function add($params){
                 
        $params = $this->prepareParams($params);
        if($params == false){return false;}
                
        $tpl = $params['tpl'];
        $name = $params['name'];
        $description = $params['description'];
        $text_button = $params['text_button'];
        $link = $params['link'];
        $company_id = $params['company_id'];
        $size = $params['size'];//not in params        
        $user_id = $params['user_id'];//not in params
        $moderation = $params['moderation'];//not in params
        $type_target = $params['type_target'];//not in params
        $block = $params['block'];//not in params
          
        $ads_id = $this->insert(
            $insert=array('product_template' => $tpl,
            'name' => $name, 'size' => $size, 'company_id' => $company_id,
            'link' => $link, 'moderation' => $moderation, 'user_id' => $user_id,
            'type_target' => $type_target, 'block' => $block,
            'description' => $description, 'text_button' => $text_button),
            $table='ads',
            $prefix=0); 
        if($ads_id != true){
            $this->setError('Error insert ads.');
            return false;
        }
        return $ads_id;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function del($params){
        $email=$params['email'];
        $password=$params['password'];
                
        #validate target_id
        $ads_id = $this->validateAdsId($params['ads_id'], $setError=1);
        if($ads_id === false){return false;}      
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $ads_id, $authLvl=1) === false){return false;}
                
        #update ads
        $res = $this->update($update = array('deleted'=>'1'),
            $table='ads',
            $where_pattern='id=:?',
            $where_params = array($ads_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update ads.');
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $ads_id
     * @return boolean
     */
    protected function validateAdsId($ads_id, $setError=1){
        $ads_id=$this->toInt($ads_id);
        if($ads_id === false){
            if($setError == 1){
                $this->setErrors('Invalid ads_id.');
            }
            return false;            
        }
        
        return $ads_id;
    }
        
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function getActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $ads_id=(int)$params['ads_id'];       
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $ads_id, $authLvl=1) === false){return false;}
        
        $row = $this->select($select=array('a.runing as activity'),
        $table='ads as a',
        $where_pattern='a.id=:?',
        $where_params=array($ads_id),
        $type='one',
        $prefix=0,
        $join='');
        
        if($row == false){
            $this->setErrors('No this ads.');
            return false;
        }
         
        return $row['activity'];
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function setActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $ads_id=(int)$params['ads_id'];
                
        #validate $active
        $active = $this->toInt($params['activity']);
        if($active !== 1 AND $active !== 0){
            $this->setErrors('Invalid status.');
            return false;
        }
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
                
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $ads_id, $authLvl=1) === false){return false;}
        
        #update target
        $res = $this->update($update = array('runing'=>$active),
            $table='ads',
            $where_pattern='id=:?',
            $where_params = array($ads_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update ads.');
            return false;
        }
        
        return true;
    }
    
    protected function prepareParams($params){
        
        $email = $params['email'];
        $password = $params['password'];        
        //echo 'prepareParams $email: '.$email.'<br>';
        //echo '$password: '.$password.'<br>';
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        $tpl = (int)$params['tpl'];
        $link = $params['link'];

        $company_id = (int)$params['company_id'];
        $name = $this->getBCore()->textSave($params['name'], BANNER_GENERATOR_TITLE_MAXLENGTH);
        $description = $this->getBCore()->textSave($params['description'], BANNER_GENERATOR_DESCRIPTION_MAXLENGTH);
        $text_button = $this->getBCore()->textSave($params['text_button'], BANNER_GENERATOR_BUTTON_MAXLENGTH);
        $moderation = 1;
        $type_target = 3;
        $block = 1;

        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $company_id, $authLvl=0) === false){return false;}
         
        $company = new \models\company();
        
        $row = $company->get($params); //$email, $password, $company_id
        if(empty($row['domain'])){
            $this->setErrors('Domain in bd empty.');
            return false;
        }
        
        if(!empty($link)){
            //удаление первого символа если он является слешем
            $link_first_char = substr($link, 0, 1);
            if($link_first_char == '/'){
                $link = substr($link, 1, strlen($link) - 1);            
            }

            if(substr_count($link, $row['domain']) > 0){
                $this->setErrors('link should not contain the domain.');
                return false;
            }

            $link = '/'.$link;
            $link_with_domain = 'http://'.$row['domain'].$link;
            $bCore = $this->getBCore();        
            $link_with_domain = $bCore::idna()->encode($link_with_domain);  
            //echo '$link: '.$link_with_domain;
            if(filter_var($link_with_domain, FILTER_VALIDATE_URL)){
                $link = htmlspecialchars(trim($link));
                $link = str_replace('&amp;', '&', $link);
            }else{
                $this->setErrors('Link is invalid');
                return false;
            }
            //echo '$link: ' .$link;
        }
        
        
        if(empty($name)){
            $this->setErrors('Not set name.');
            return false;
        }
        
        if(empty($description)){
            $this->setErrors('Not set description.');
            return false;
        }

        if(empty($text_button)){
            $this->setErrors('Not set text_button.');
            return false;
        }
        
        __autoload('/models/generatorTemplate.php');
        $generatorTemplate = new \models\generatorTemplate();
        if($generatorTemplate->isTemplate($tpl, $user_id) == false){
            return false;
        }
        
        if(empty($tpl)){
            $this->setErrors('Not set template id.');
            return false;
        }
                       
        $template = $generatorTemplate->get($params);
        if(empty($template['size'])){        
            $this->setErrors('Failed to get the size of template.');
            return false;
        }
        $size = $template['size'];
        
        $params['size'] = $size;
        $params['tpl'] = $tpl;
        $params['user_id'] = $user_id;
        $params['name'] = $name;
        $params['description'] = $description;
        $params['text_button'] = $text_button;
        $params['link'] = $link;
        $params['company_id'] = $company_id;
        $params['moderation'] = $moderation;
        $params['type_target'] = $type_target;
        $params['block'] = $block;
        
        return $params;
    }
    
    public function getCompanyId($ads_id, $setError = 0){
        $ads_id = (int)$ads_id;
        $row = $this->select($select=array('a.company_id'),
        $table='ads as a', $where_pattern='a.id=:?',
        $where_params=array($ads_id),
        $type='one',
        $prefix=0,
        $join='');
        
        if($row['company_id'] == false){
            if($setError == 1){
                $this->setErrors('No such ads.');
            }
            return false;
        }
        
        return $row;
    }
    
    public function edit($params){
        $ads_id = (int)$params['ads_id'];
        if(empty($ads_id)){
            $this->setErrors('ads_id no set.');
            return false;
        }
        
        $row = $this->getCompanyId($ads_id);
        
        if($row['company_id'] == false){
            $this->setErrors('No such ads.');
            return false;
        }
        $params['company_id'] = $row['company_id'];
        
        $params = $this->prepareParams($params);
        if($params == false){return false;}
                
        
        $tpl = $params['tpl'];        
        $name = $params['name'];
        $description = $params['description'];
        $text_button = $params['text_button'];
        $link = $params['link'];
        $user_id = $params['user_id'];#not in params
        $size = $params['size'];#not in params
        $company_id = $params['company_id'];#not in params
        $moderation = $params['moderation'];#not in params
        $type_target = $params['type_target'];#not in params
        $block = $params['block'];#not in params
        
        $res = $this->update($update = array('size'=>$size,'product_template'=>$tpl,'user_id'=>$user_id,
            'name'=>$name,'description'=>$description,'text_button'=>$text_button,
            'link'=>$link,'company_id'=>$company_id,'moderation'=>$moderation,
            'type_target'=>$type_target,'block'=>$block,), $table='ads',
            $where_pattern='id=:?', $where_params = array($ads_id), $prefix = 0);
        
        if($res != true){
            $this->setErrors('Error update ads.');
            return false;
        }
        return $res;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function get($params){
        #params
        $email = $params['email'];
        $password = $params['password']; 
        $company_id = (int)$params['company_id'];
        
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}      

        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $company_id, $authLvl=0) === false){return false;}
                        
        $row = $this->select($select=array('a.id as id','a.product_template as tpl','a.moderation',
            'a.name','a.description','a.text_button','s.description as size','a.link'),
        $table='ads as a', $where_pattern='a.deleted = 0 AND a.type_target = 3
            AND a.company_id=:?',
        $where_params=array($company_id), $type='all', $prefix=0,
        $join='LEFT JOIN sizes as s ON s.size = a.size');
        
        if($row == false){
            $this->setErrors('This company no have ads.');
            return false;
        }
        
        return $row;
    }
}