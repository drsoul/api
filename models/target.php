<?php
namespace models;

defined('EXEC') or die('No direct access!');

class target extends \core\model{
    
    
    protected function prepareParams($params){
                        
        $email=$params['email'];
        $password=$params['password'];
        
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        $site=$params['site'];                 
        $geo = 0;
        $time_active = 0;   
        
        $paramsInt['ads_id']=$params['ads_id'];
        $paramsInt['sex']=$params['sex'];
        $paramsInt['age']=$params['age'];
        $paramsInt['interest']=$params['interest'];
        $paramsInt['cpm']=$params['cpm'];
        $paramsInt['max_sum_hour']=$params['max_sum_hour'];
        $paramsInt['max_sum_day']=$params['max_sum_day'];
        $paramsInt['views_per_user']=$params['views_per_user'];
        $paramsInt['views_hour_limit']=$params['views_hour_limit'];
        $paramsInt['views_day_limit']=$params['views_day_limit'];
        $paramsInt['retarget']=$params['retarget'];
        $paramsInt['device']=$params['device'];

        $paramsIntResult=$this->testIntParams($paramsInt, $setError=1, $setFalse=1);
        if($paramsIntResult === false){return false;}
        
        $ads_id=$paramsIntResult['ads_id'];
        $sex=$paramsIntResult['sex'];
        $age=$paramsIntResult['age'];
        $interest=$paramsIntResult['interest'];
        $cpm=$paramsIntResult['cpm'];
        $max_sum_hour=$paramsIntResult['max_sum_hour'];
        $max_sum_day=$paramsIntResult['max_sum_day'];
        $views_per_user=$paramsIntResult['views_per_user'];
        $views_hour_limit=$paramsIntResult['views_hour_limit'];
        $views_day_limit=$paramsIntResult['views_day_limit'];
        $retarget=$paramsIntResult['retarget']; 
        $device=$paramsIntResult['device'];
        
        #validate $retarget_days 
        if($retarget == 1){
            $retarget_days=$this->toInt($params['retarget_days']);
            if($retarget_days === false OR $retarget_days < 0 OR $retarget_days > 30){
                $this->setErrors('Invalid retarget_days. Need 0 < retarget_days < 31.');
                return false;
            }
        }   
        
        #validate retarget        
        if($retarget !== 1 AND $retarget !== 0){
            $this->setErrors('Retarget is invalid.');
            return false;
        }
        
        #prepare region, time
        $time_zone=0;
        __autoload('/config/apiConfig.php');
        if(\config\apiConfig::I()->getMethodHTTP() == 'GET'){
            $region=urldecode($params['region']);
            $time=urldecode($params['time']);
        }else{
            $region=$params['region'];
            $time=$params['time'];
        }         
        
        #validate site
        if(!empty($site)){
            $site_arr = explode(',', $site);
            $sep = '';
            $domain_tmp = '';
            foreach($site_arr as $key => $value){
                $domain_tmp = $this->validateDomain($value);
                if($domain_tmp == false){
                    $this->setErrors('Site is invalid.');
                    return false;
                }
                $domain_str = $sep.$domain_tmp;
                $sep = ',';
            }
        }
        
        #validate region
        if(!empty($region)){
            $region=json_decode($region);
            if($region == false){
                $this->setErrors('Region is invalid json.');
                return false;
            }
        }
        
        #validate time
        if(!empty($time)){
            
            $time_raw = json_decode($time);

            if($time_raw == false){
                $this->setErrors('Time is invalid json.');
                return false;
            }
            $time = $time_raw->time;            
            
            if(empty($time)){
                $this->setErrors('Time is empty.');
                return false;
            }
            $time_zone = $this->toInt($time_raw->time_zone);
            if($time_zone === false){
                $this->setErrors('No set time_zone');
                return false;
            }
        }        

                
        #authorization user to company, ads, target
        $userAuthComAdsTar = $this->userAuthComAdsTar($user_id, $ads_id, $authLvl=1);
        if($userAuthComAdsTar === false){return false;}
        $company_id = $userAuthComAdsTar['company_id'];
        
        //init company domain for retarget
        $params['company_site_id'] = '';
        if($retarget === 1){
            $site_id = $this->getSiteIdByCompanyId($company_id, $setError=1);
            if($domain === false){return false;}
            $params['company_site_id']=$site_id;     
        }
                
        $params['ads_id']=$ads_id;
        $params['sex']=$sex;
        $params['age']=$age;
        $params['interest']=$interest;
        $params['cpm']=$cpm;
        $params['device']=$device;        
        $params['max_sum_hour']=$max_sum_hour;
        $params['max_sum_day']=$max_sum_day;
        $params['views_per_user']=$views_per_user;
        $params['views_hour_limit']=$views_hour_limit;
        $params['views_day_limit']=$views_day_limit;
        $params['retarget']=$retarget;
        $params['time_zone']=$time_zone;
        $params['site']=$site;
        $params['region']=$region;
        $params['time']=$time;        
        
        $params['company_id']=$company_id;
        
        $params['geo']=$geo;
        $params['time_active']=$time_active;
        
        $params['retarget_days']=$retarget_days;
        $params['user_id']=$user_id;
                
        return $params;
    }
    
    /**
     * 
     * @param type $company_id
     * @param type $setError
     * @return boolean
     */
    protected function getSiteIdByCompanyId($company_id, $setError=1){
            $company_id=(int)$company_id;
            if(empty($company_id)){
                if($setError==1){
                    $this->setErrors('No set company_id (getSiteIdByCompanyId).');
                }
                return false;
            }
        
            #select target
            $row = $this->select($select=array('u2s.site_id'),
            $table='company as c',
            $where_pattern='c.id=:?',
            $where_params=array($company_id),
            $type='one',
            $prefix=0,
            $join='LEFT JOIN user2site as u2s ON u2s.id = c.user2site_id');

            if($row == false){
                if($setError==1){
                    $this->setErrors('Error get site_id for retarget.');
                }
                return false;
            }
            
            return $row['site_id'];
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function add($params){
   
        $params = $this->prepareParams($params);
        if($params == false){return false;}
        
        $ads_id=$params['ads_id'];
        $sex=$params['sex'];
        $age=$params['age'];
        $interest=$params['interest'];
        $cpm=$params['cpm'];
        $device=$params['device'];        
        $max_sum_hour=$params['max_sum_hour'];
        $max_sum_day=$params['max_sum_day'];
        $views_per_user=$params['views_per_user'];
        $views_hour_limit=$params['views_hour_limit'];
        $views_day_limit=$params['views_day_limit'];
        $retarget=$params['retarget'];//доделать
        $time_zone=$params['time_zone'];
        $site=$params['site'];
        $region=$params['region'];
        $time=$params['time'];
                
        $company_id=$params['company_id'];
        
        $geo=$params['geo'];
        $time_active=$params['time_active'];
        
        $company_site_id=$params['company_site_id'];
        $user_id=$params['user_id'];        
        $retarget_days=$params['retarget_days'];
        
        #insert target(blank)
        $target_id = $this->insert(
            $insert=array('ads_id' => $ads_id,
            'active' => 1, 'retarget' => $retarget),
            $table='target',
            $prefix=0); 
        if($target_id != true){
            $this->setErrors('Error insert target.');
            return false;
        }
        
        #insert time
        $res = $this->insertTime($time, $target_id);
        if($res === false){
            return false;
        }elseif($res === 1){
            $time_active = $res;
        }        
        
        #insert geo
        $res = $this->insertGeo($region, $target_id);
        if($res === false){
            return false;
        }elseif($res === 1){
            $geo = $res;
        }        
        
        #update target
        $res = $this->update($update = array('sex'=>$sex,'age'=>$age,
            'interest'=>$interest, 'cpm'=>$cpm,'device_id'=>$device,
            'max_sum_hour'=>$max_sum_hour, 'max_sum_day'=>$max_sum_day,
            'views_per_user'=>$views_per_user, 'views_hour_limit'=>$views_hour_limit,
            'views_day_limit'=>$views_day_limit,'retarget'=>$retarget,'time_zone'=>$time_zone,
            'site'=>$site, 'time_active' => $time_active, 'geo' => $geo),
            $table='target',
            $where_pattern='id=:?',
            $where_params = array($target_id),
            $prefix = 0);        
                
        if($res == false){
            $this->setErrors('Error update table(target) by target_id: '.$target_id . '.');
        }
        
        #retarget insert
        if($retarget === 1){
            $this->insertRetargetSites($retarget_days, $company_site_id, $user_id, $ads_id, $setError=1);            
        }
        
        return $target_id;
    }
    
    /**
     * 
     * @param type $retarget_days
     * @param type $company_site_id
     * @param type $user_id
     * @param type $ads_id
     * @return boolean
     */
    protected function insertRetargetSites($retarget_days, $company_site_id, $user_id, $ads_id, $setError=1){

        if(empty($retarget_days)){
            if($setError == 1){
                $this->setErrors('Empty retarget_days (insertRetargetSites).');
            }
            return false;
        }
        
        if(empty($ads_id)){
            if($setError == 1){
                $this->setErrors('Empty ads_id (insertRetargetSites).');
            }
            return false;
        }
        
        if(empty($user_id)){
            if($setError == 1){
                $this->setErrors('Empty user_id (insertRetargetSites).');
            }
            return false;
        }
        
        if(empty($company_site_id)){
            if($setError == 1){
                $this->setErrors('Empty company_site_id (insertRetargetSites).');
            }
            return false;
        }

        #select retarget_sites
        $retarget_sites_row = $this->select($select=array('check_counter'),
            $table='retarget_sites',
            $where_pattern='site_id=:?',
            $where_params=array($company_site_id),
            $type='one',
            $prefix=0,
            $join='');
        
        if($retarget_sites_row['check_counter'] === false){$check_counter = 0;}
        else{$check_counter = (int)$retarget_sites_row['check_counter'];}

        #insert retarget_sites
        $retarget_sites_id = $this->insert(
            $insert=array('ads_id' => $ads_id,
            'site_id' => $company_site_id, 'days' => $retarget_days, 'user_id' => $user_id,
            'check_counter' => $check_counter),
            $table='retarget_sites',
            $prefix=0); 
        
        if($retarget_sites_id === false){
            if($setError == 1){
                $this->setError('Error insert retarget_sites.');
            }
            return false;
        }
        
        return $retarget_sites_id;
    }
    
    /**
     * 
     * @param type $time
     * @param type $target_id
     * @return boolean|int
     */
    protected function insertTime($time, $target_id){
        #insert time
        if(!empty($time)){
                        
            $time = $this->filterArray($time, $arrayFilter = array('day_id', 'time_id'),
                $addElem = array('target_id' => $target_id), $intValue = 1);
            if($time == false){
                $this->setErrors('Time array is invalid.');
                return false;
            }
            
            $res = $this->insertMass($time, $table='time_target', $prefix = 0, $onDuplicateKey = '');
            if($res == false){
                $this->setErrors('Error insert time.');
                return false;
            }
            $time_active = 1;
            return $time_active;
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $region
     * @param type $target_id
     * @return boolean|int
     */
    protected function insertGeo($region, $target_id){
        #insert region        
        if(!empty($region)){
            
            $region = $this->filterArray($region,
                $arrayFilter = array('country_id', 'area_id', 'region_id', 'city_id'),
                $addElem = array('target_id' => $target_id), $intValue = 1);
            if($region == false){
                $this->setErrors('Region array is invalid.');
                return false;
            }   
            
            $res = $this->insertMass($region, $table='target_geo', $prefix = 0, $onDuplicateKey = '');
            if($res == false){
                $this->setErrors('Error insert region.');
                return false;
            }
            $geo = 1;
            return $geo;
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function edit($params){
        
        $target_id=(int)$params['target_id'];
        if(empty($target_id)){
            $this->setErrors('target_id not set.');
            return false;
        }
        
        $ads = $this->getAdsId($target_id, $setError = 1);        
        if($ads['ads_id'] == false){return false;}
        $params['ads_id'] = $ads['ads_id'];
        
        $params = $this->prepareParams($params);
        if($params == false){return false;}
        
        $ads_id=$params['ads_id'];
        $sex=$params['sex'];
        $age=$params['age'];
        $interest=$params['interest'];
        $cpm=$params['cpm'];
        $device=$params['device'];        
        $max_sum_hour=$params['max_sum_hour'];
        $max_sum_day=$params['max_sum_day'];
        $views_per_user=$params['views_per_user'];
        $views_hour_limit=$params['views_hour_limit'];
        $views_day_limit=$params['views_day_limit'];
        $retarget=$params['retarget'];//доделать
        $time_zone=$params['time_zone'];
        $site=$params['site'];
        $region=$params['region'];
        $time=$params['time'];
                
        $company_id=$params['company_id'];
        
        $geo=$params['geo'];
        $time_active=$params['time_active'];
        
        $company_site_id=$params['company_site_id'];
        $user_id=$params['user_id'];        
        $retarget_days=$params['retarget_days'];
        
        #delete target_geo
        $res = $this->delete($table = 'target_geo',
                $where_pattern='target_id=:?',
                $where_params = array($target_id),
                $prefix = 0);
        if($res == false){return false;}
        
        #delete time_target
        $res = $this->delete($table = 'time_target',
                $where_pattern='target_id=:?',
                $where_params = array($target_id),
                $prefix = 0);
        if($res == false){return false;}
        
        #insert time
        $res = $this->insertTime($time, $target_id);
        if($res === false){
            return false;
        }elseif($res === 1){
            $time_active = $res;
        }        
        
        #insert geo
        $res = $this->insertGeo($region, $target_id);
        if($res === false){
            return false;
        }elseif($res === 1){
            $geo = $res;
        }   
        
        #update target
        $res = $this->update($update = array('sex'=>$sex,'age'=>$age,
            'interest'=>$interest, 'cpm'=>$cpm,'device_id'=>$device,
            'max_sum_hour'=>$max_sum_hour, 'max_sum_day'=>$max_sum_day,
            'views_per_user'=>$views_per_user, 'views_hour_limit'=>$views_hour_limit,
            'views_day_limit'=>$views_day_limit,'retarget'=>$retarget,'time_zone'=>$time_zone,
            'site'=>$site, 'time_active' => $time_active, 'geo' => $geo),
            $table='target',
            $where_pattern='id=:?',
            $where_params = array($target_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update ads.');
            return false;
        }
        
        #retarget delete insert
        if($retarget === 1){ 
            
            if($this->deleteRetargetSites($ads_id, $setError = 1) === false){return false;}

            if($this->insertRetargetSites($retarget_days, $company_site_id, $user_id, $ads_id, $setError=1) === false){
                return false;
            }            
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $ads_id
     * @param type $setError
     * @return boolean
     */
    protected function deleteRetargetSites($ads_id, $setError = 1){
        
        if(empty($ads_id)){            
            if($setError == 1){
                $this->setErrors('Empty ads_id (deleteRetargetSites).');
            }
            return false;
        }
        
        #delete target
        $res = $this->delete($table = 'retarget_sites',
                $where_pattern='ads_id=:?',
                $where_params = array($ads_id),
                $prefix = 0);
        if($res == false){
            if($setError == 1){
                $this->setErrors('Error delete retarget_sites.');
            }
            return false;            
        }        
        return true;
    }
    
    /**
     * 
     * @param type $target_id
     * @return boolean
     */
    protected function validateTargetId($target_id, $setError=1){
        $target_id=$this->toInt($target_id);
        if($target_id === false){
            if($setError == 1){
                $this->setErrors('Invalid target_id.');
            }
            return false;            
        }
        
        return $target_id;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function del($params){
        $email=$params['email'];
        $password=$params['password'];
        
        #validate target_id
        $target_id = $this->validateTargetId($params['target_id'], $setError=1);
        if($target_id === false){return false;}
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id === false){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $target_id, $authLvl=2) === false){return false;}
        
        #delete target_geo
        if($this->deleteGeo($target_id, $setError=1) === false){return false;}
        
        #delete time_target
        if($this->deleteTime($target_id, $setError=1) === false){return false;}
        
        #delete target
        if($this->deleteTarget($target_id, $setError=1) === false){return false;}
        
        return true;        
    }
    
    /**
     * 
     * @param type $target_id
     * @param type $setError
     * @return boolean
     */
    protected function deleteTarget($target_id, $setError=1){
        #delete target
        $res = $this->delete($table = 'target',
                $where_pattern='id=:?',
                $where_params = array($target_id),
                $prefix = 0);
        if($res == false){
            if($setError == 1){
                $this->setErrors('Error delete target.');
            }
            return false;            
        } 
        return true;
    }
    
    /**
     * 
     * @param type $target_id
     * @param type $setError
     * @return boolean
     */
    protected function deleteTime($target_id, $setError=1){
        #delete time_target
        $res = $this->delete($table = 'time_target',
                $where_pattern='target_id=:?',
                $where_params = array($target_id),
                $prefix = 0);
        if($res == false){
            if($setError == 1){
                $this->setErrors('Error delete time_target.');
            }
            return false;            
        }
        return true;
    }
    
    /**
     * 
     * @param type $target_id
     * @return boolean
     */
    protected function deleteGeo($target_id, $setError=1){        
        #delete target_geo
        $res = $this->delete($table = 'target_geo',
                $where_pattern='target_id=:?',
                $where_params = array($target_id),
                $prefix = 0);
        if($res == false){
            if($setError == 1){
                $this->setErrors('Error delete target_geo.');
            }
            return false;            
        }        
        return true;
    }


    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function getActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $target_id=(int)$params['target_id'];       
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}

        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $target_id, $authLvl=2) === false){return false;}

        #select target
        $row = $this->select($select=array('t.active as activity'),
        $table='target as t',
        $where_pattern='t.id=:?',
        $where_params=array($target_id),
        $type='one',
        $prefix=0,
        $join='');
        
        if($row == false){
            $this->setErrors('No this target.');
            return false;
        }
         
        return $row['activity'];
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function setActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $target_id=(int)$params['target_id'];
                
        #validate $active
        $active = $this->toInt($params['activity']);
        if($active !== 1 AND $active !== 0){
            $this->setErrors('Invalid status.');
            return false;
        }
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
  
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $target_id, $authLvl=2) === false){return false;}
        
        #update target
        $res = $this->update($update = array('active'=>$active),
            $table='target',
            $where_pattern='id=:?',
            $where_params = array($target_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update target.');
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function get($params){
        $email = $params['email'];
        $password = $params['password']; 
        $ads_id = (int)$params['ads_id'];
        
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $ads_id, $authLvl=1) === false){return false;}
        
        #select target
        $row = $this->select($select=array('t.id as id', 't.sex as sex', 't.interest as interest',
            't.cpm as cpm', 't.device_id as device_id', 't.max_sum_hour as max_sum_hour',
            't.max_sum_day as max_sum_day', 't.views_per_user as views_per_user',
            't.views_hour_limit as views_hour_limit', 't.views_day_limit as views_day_limit',
            't.retarget as retarget', 't.time_zone as time_zone', 't.site as site',
            't.time_active as time_active', 't.geo as geo'),
        $table='target as t',
        $where_pattern='t.ads_id=:?',
        $where_params=array($ads_id),
        $type='all',
        $prefix=0,
        $join='');
        
        if($row == false){
            $this->setErrors('This ads no have target.');
            return false;
        }
        
        #add time
        $row = $this->addTime($row);
        
        #add geo
        $row = $this->addGeo($row);     
        
        return $row;
    }
    
    protected function addTime($row){
        
        $rows_plus_time = array();
        foreach($row as $key => $value){
            $row_1 = $this->select($select=array('tt.day_id', 'tt.time_id'),
            $table='time_target as tt',
            $where_pattern='tt.target_id=:?',
            $where_params=array($value['id']),
            $type='all',
            $prefix=0,
            $join='');
            
            $value_tmp = $value;
            $value_tmp['time_array'] = $row_1;
            $rows_plus_time[] = $value_tmp;
        }
        
        return $rows_plus_time;
    }
    
    protected function addGeo($row){
        
        $rows_plus_geo = array();
        foreach($row as $key => $value){
            $row_1 = $this->select($select=array('tg.country_id', 'tg.area_id',
                'tg.region_id', 'tg.city_id'),
            $table='target_geo as tg',
            $where_pattern='tg.target_id=:?',
            $where_params=array($value['id']),
            $type='all',
            $prefix=0,
            $join='');
            
            $value_tmp = $value;
            $value_tmp['geo_array'] = $row_1;
            $rows_plus_geo[] = $value_tmp;
        }
        
        return $rows_plus_geo;
    }

    public function getAdsId($target_id, $setError = 0){
        $target_id = (int)$target_id;
        $row = $this->select($select=array('t.ads_id'),
        $table='target as t',
        $where_pattern='t.id=:?',
        $where_params=array($target_id),
        $type='one',
        $prefix=0,
        $join='');
        
        if($row['ads_id'] == false){
            if($setError == 1){
                $this->setErrors('No such target.');
            }
            return false;
        }
        
        return $row;
    }
}