<?php
namespace models;

defined('EXEC') or die('No direct access!');

class generatorTemplate extends \core\model{
  
    /**
     * 
     * @param type $tpl
     * @param type $user_id
     * @return boolean
     */
    public function isTemplate($tpl = 0, $user_id = 0){       

        $tpl = (int)$tpl;
        $user_id = (int)$user_id;
        
        if(empty($tpl)){
            $this->setErrors('Template id is empty.');
            return false;
        } 
        
        if(empty($user_id)){
            $this->setErrors('generatorTemplate\isTemplate authorization failed.');
            return false;
        }  
        
        #SELECT generator_template
        $row = $this->select($select=array('id'),
                $table='generator_templates',
                $where_pattern='id=:? AND (user_id = 0 OR user_id = '.$user_id.')',
                $where_params=array($tpl),
                $type='one',
                $prefix=0,
                $join='');
        
        if(empty($row['id'])){
            $this->setErrors('No template with this id.');
            return false;
        }           
        return true;        
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function get($params){       
        
        #params
        $email = $params['email'];    
        $password = $params['password'];
        $tpl = (int)$params['tpl'];
                
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
                         
        $type = 'all'; 
        $where_pattern='gt.user_id = 0 OR gt.user_id = '.$user_id;
        if(!empty($tpl)){
            $type = 'one';  
            $where_pattern='gt.id=:? AND (gt.user_id = 0 OR gt.user_id = '.$user_id.')';
        } 
        
        #SELECT generator_template
        $rows = $this->select($select=array('gt.id', 's.size', 's.description', 'gt.name', 'gt.user_id'),
                $table='generator_templates as gt',
                $where_pattern,
                $where_params=array($tpl),
                $type,
                $prefix=0,
                $join='INNER JOIN sizes as s ON gt.size = s.size');
        if($rows == false){
            $this->setErrors('This user no have template(s).');
            return false;
        }           
        return $rows;        
    }    
}