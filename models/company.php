<?php
namespace models;

defined('EXEC') or die('No direct access!');

class company extends \core\model{
        
    /**
     * 
     * @param type $email
     * @param type $password
     * @param type $domain
     * @return boolean
     */
    public function add($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $domain=$params['domain'];
        
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        if(empty($domain)){
            $this->setErrors('Domain empty.');
            return false;
        }
        
        $preg = '~^http://(www.)?~i';
        $site = preg_replace($preg, '', $domain);
        $site = explode("/", $site);
        $site = $site[0];
        $company_name = $site;
        $site = \Core::idna()->encode($site);

        if(empty($site)){
            $this->setErrors('Site empty.');
            return false;
        }

        if(!\Core::I()->is_valid_domain_name($site)){
            $this->setErrors('Domain not valid.');
            return false;
        }
                
        #SELECT site
        $row = $this->select($select = array('site_id'),
                $table = 'site',
                $where_pattern = 'domain=:?',
                $where_params = array($site),
                $type = 'one',
                $prefix = 0);
        
        $site_id = $row['site_id'];
        if( empty($site_id) ){
            $site_id = (int)file_get_contents('http://www.megaindex.ru/site_api.php?site=' . $site);
        
            if(empty($site_id) OR $site_id == 0){
                $this->setErrors('Failed to extract site_id from megaindex api.');
                return false;
            }

            #INSERT site
            $this->insert($insert = array('site_id' => $site_id, 'domain' => $site),
                    $table = 'site',
                    $prefix = 0);
            
            $site_id = $this->lastId();

            if(empty($site_id) OR $site_id == 0){
                $this->setErrors('Failed insert site.');
                return false;
            }
        }

        #SELECT user2site
        $row = $this->select($select = array('id'),
                $table = 'user2site',
                $where_pattern = 'user_id=:? AND site_id=:?',
                $where_params = array($user_id, $site_id),
                $type = 'one',
                $prefix = 0);
        
               
        #INSERT user2site
        if(empty($row['id'])) {            

            $user2site_id = $this->insert($insert = array('user_id' => $user_id, 'site_id' => $site_id),
                    $table = 'user2site',
                    $prefix = 0); 
            
            if($user2site_id != true){return false;}    
            
        }else{
            $user2site_id = $row['id'];
        }
        
        #INSERT company
        $company_id = $this->insert($insert = array('name' => $site, 'runing' => '1',
            'start' => '', 'end' => '', 'user2site_id' => $user2site_id),
                $table = 'company',
                $prefix = 0,
                $onDuplicateKey = "deleted='0'"); 
        
        if($company_id != true){return false;}     
        
        return $company_id;
    }
    
    /**
     * 
     * @param type $email
     * @param type $password
     * @param type $company_id
     * @return boolean
     */
    public function get($params){     
        
        $email=$params['email'];
        $password=$params['password'];
        $company_id=(int)$params['company_id'];
        
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
                
        $type='all';
        $where_pattern='company.deleted=0 AND user2site.user_id=:?';
        $where_params=array($user_id);
        if(!empty($company_id)){
            $type='one';
            $where_pattern='company.deleted=0 AND user2site.user_id=:? AND company.id=:?';
            $where_params=array($user_id, $company_id);
        }
        
        #SELECT company user2site
        $rows = $this->select($select=array('company.id','site.domain','company.name','company.balance'),
                $table='company',
                $where_pattern,
                $where_params,
                $type,
                $prefix=0,
                $join='LEFT JOIN user2site ON user2site.id = company.user2site_id
                    LEFT JOIN site ON site.site_id = user2site.site_id');

        if(empty($rows)){
            $this->setErrors('This user no have companies.');
            return false;
        }      
                
        return $rows;        
    }
    
    public function userOwnCompany($user_id, $company_id, $setError = 0){
        
        $user_id = (int)$user_id;
        $company_id = (int)$company_id;
        #SELECT company user2site
        $row = $this->select($select=array('company.id as id','company.name','company.balance'),
                $table='company',
                $where_pattern='user2site.user_id=:? AND company.id=:?',
                $where_params=array($user_id, $company_id),
                $type='one',
                $prefix=0,
                $join='LEFT JOIN user2site ON user2site.id = company.user2site_id');

        if(empty($row['id'])){ 
            if($setError == 1){
                $this->setErrors('This user no have this company.');
            }
            return false;
        }      
                
        return true;        
    }
    
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function del($params){
        $email=$params['email'];
        $password=$params['password'];
                
        #validate target_id
        $company_id = $this->validateCompanyId($params['company_id'], $setError=1);
        if($company_id === false){return false;}      
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $company_id, $authLvl=0) === false){return false;}
                
        #update company
        $res = $this->update($update = array('deleted'=>'1'),
            $table='company',
            $where_pattern='id=:?',
            $where_params = array($company_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update company.');
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * @param type $company_id
     * @return boolean
     */
    protected function validateCompanyId($company_id, $setError=1){
        $company_id=$this->toInt($company_id);
        if($company_id === false){
            if($setError == 1){
                $this->setErrors('Invalid company_id.');
            }
            return false;            
        }
        
        return $company_id;
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function getActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $company_id=(int)$params['company_id'];       
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
                      
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $company_id, $authLvl=0) === false){return false;}
 
        $row = $this->select($select=array('c.runing as activity'),
        $table='company as c',
        $where_pattern='c.id=:?',
        $where_params=array($company_id),
        $type='one',
        $prefix=0,
        $join='');
        
        if($row == false){
            $this->setErrors('No this company.');
            return false;
        }
         
        return $row['activity'];
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function setActivity($params){
        
        $email=$params['email'];
        $password=$params['password'];
        $company_id=(int)$params['company_id'];
                
        #validate $active
        $active = $this->toInt($params['activity']);
        if($active !== 1 AND $active !== 0){
            $this->setErrors('Invalid activity.');
            return false;
        }
        
        #authorization
        $user_id = $this->authorization($email, $password);
        if($user_id != true){return false;}
        
        #authorization user to company, ads, target
        if($this->userAuthComAdsTar($user_id, $company_id, $authLvl=0) === false){return false;}
                        
        #update target
        $res = $this->update($update = array('runing'=>$active),
            $table='company',
            $where_pattern='id=:?',
            $where_params = array($company_id),
            $prefix = 0); 
        
        if($res != true){
            $this->setErrors('Error update company.');
            return false;
        }
        
        return true;
    }
}