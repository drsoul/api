<?php
namespace models;

defined('EXEC') or die('No direct access!');

class geo extends \core\model{
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    public function get($params){
        
        $size = $params['size'];
        
        if($size == 'location'){$res = $this->getLocation($params); return $res;}
        elseif($size == 'country'){$res = $this->getCountry($params); return $res;}
        elseif($size == 'area'){$res = $this->getArea($params); return $res;}
        elseif($size == 'region'){$res = $this->getRegion($params); return $res;}
        elseif($size == 'city'){$res = $this->getCity($params); return $res;}
        else{
            $this->setErrors('Invalid size.');
            return false;
        }
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    protected function getLocation($params){      
        
        #SELECT ip_location
        $rows = $this->select($select=array('id', 'name'),
                $table='ip_location',
                $where_pattern='1',
                $where_params=array(),
                $type = 'all',
                $prefix=0,
                $join='');
        if($rows == false){
            $this->setErrors('Error select location.');
            return false;
        }           
        return $rows;        
    }    
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    protected function getCountry($params){   
        
        $location_id = $this->toInt($params['location_id']);
        if($location_id === false){
            $this->setErrors('location_id is empty.');
            return false;            
        }
        
        #SELECT ip_country
        $rows = $this->select($select=array('id', 'name', 'name_ru', 'location_id'),
                $table='ip_country',
                $where_pattern='location_id=:?',
                $where_params=array($location_id),
                $type = 'all',
                $prefix=0,
                $join='');
        if($rows == false){
            $this->setErrors('Error select country.');
            return false;
        }           
        return $rows;        
    }  
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    protected function getArea($params){   
        
        $country_id = $this->toInt($params['country_id']);
        if($country_id === false){
            $this->setErrors('country_id is empty.');
            return false;            
        }
        
        #SELECT ip_area
        $rows = $this->select($select=array('id', 'name', 'country_id'),
                $table='ip_area',
                $where_pattern='country_id=:?',
                $where_params=array($country_id),
                $type = 'all',
                $prefix=0,
                $join='');
        if($rows == false){
            $this->setErrors('Error select area.');
            return false;
        }           
        return $rows;        
    }  
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    protected function getRegion($params){   
        
        $area_id = $this->toInt($params['area_id']);
        if($area_id === false){
            $this->setErrors('area_id is empty.');
            return false;            
        }
        
        #SELECT ip_region
        $rows = $this->select($select=array('id', 'name', 'area_id'),
                $table='ip_region',
                $where_pattern='area_id=:?',
                $where_params=array($area_id),
                $type = 'all',
                $prefix=0,
                $join='');
        if($rows == false){
            $this->setErrors('Error select region.');
            return false;
        }           
        return $rows;        
    }
    
    /**
     * 
     * @param type $params
     * @return boolean
     */
    protected function getCity($params){   
        
        $region_id = $this->toInt($params['region_id']);
        if($region_id === false){
            $this->setErrors('region_id is empty.');
            return false;            
        }
        
        #SELECT ip_city
        $rows = $this->select($select=array('id', 'name', 'region_id', 'latitude', 'longitude'),
                $table='ip_city',
                $where_pattern='region_id=:?',
                $where_params=array($region_id),
                $type = 'all',
                $prefix=0,
                $join='');
        if($rows == false){
            $this->setErrors('Error select city.');
            return false;
        }           
        return $rows;        
    }
}