<?php

return array(
    'add_target' => array('class' => 'target', 'method' => 'add'),
    'edit_target' => array('class' => 'target', 'method' => 'edit'),
    'get_target' => array('class' => 'target', 'method' => 'get'),   
    'setactivity_target' => array('class' => 'target', 'method' => 'setActivity'),  
    'getactivity_target' => array('class' => 'target', 'method' => 'getActivity'),  
    'del_target' => array('class' => 'target', 'method' => 'del'),  
    
    'get_geo' => array('class' => 'geo', 'method' => 'get'),
    
    'get_generator_template' => array('class' => 'generatorTemplate', 'method' => 'get'),
    
    'add_ads' => array('class' => 'ads', 'method' => 'add'),
    'edit_ads' => array('class' => 'ads', 'method' => 'edit'),
    'get_ads' => array('class' => 'ads', 'method' => 'get'),
    'setactivity_ads' => array('class' => 'ads', 'method' => 'setActivity'),  
    'getactivity_ads' => array('class' => 'ads', 'method' => 'getActivity'), 
    'del_ads' => array('class' => 'ads', 'method' => 'del'), 
    
    'add_company' => array('class' => 'company', 'method' => 'add'),
    'get_company' => array('class' => 'company', 'method' => 'get'),
    'setactivity_company' => array('class' => 'company', 'method' => 'setActivity'),  
    'getactivity_company' => array('class' => 'company', 'method' => 'getActivity'),   
    'del_company' => array('class' => 'company', 'method' => 'del'), 
    
    'add_user' => array('class' => 'user', 'method' => 'add'),
    'get_user' => array('class' => 'user', 'method' => 'get')
);