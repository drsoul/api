<?php
namespace config;

defined('EXEC') or die('No direct access!');

class apiConfig{
    
    protected $methodHTTP = 'POST';
    protected $displayErrors = 1;
    private static $instance;
    
    
    public static function I(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self;
        }        
        return self::$instance;
    }
    
    private function __construct(){}
   
    private function __clone(){}
    
    public function getMethodHTTP(){
        return $this->methodHTTP;
    }
    
    public function displayErrors(){
        return $this->displayErrors;
    }
}